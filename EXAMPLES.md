Burpy Examples
==============

# Session #
The sessions module reads your API key and Org ID from file. The modules 
below all require the API key and most require the Org ID. You do not need 
to use the Session module, you can create your own Key and Org Id variables.

## session.Key ##
First create a text file with one line containing the API key. The filename needs to end in ".txt".

### Example: ###
<pre><code>mykey = burpy.session.Key('/home/nick/Documents/burpy/key.txt')</code></pre>

## session.OrgId ##
Create a text file with one line containing the Org Id. The filename needs to end in ".txt", it is not case-sensitive.

### Example: ###
<pre><code>myorg_id = burpy.session.OrgId('/home/nick/Documents/burpy/Org_Id.txt')</code></pre>

# pjson #
pjson stands for pretty json and prints the returned json in an easy to ready format.

### Example: ###
<pre><code>print burpy.pjson(burpy.networks.List(key=mykey, org_id=myorg_id))</code></pre>
### Return: ###
<pre><code>
[
    {
        "name": "Test_Network",
        "tags": " dev int ",
        "organizationId": "555555",
        "timeZone": "America/Los_Angeles",
        "type": "appliance",
        "id": "N_123456"
    },
    {
        "name": "Network2",
        "tags": "",
        "organizationId": "555555",
        "timeZone": "America/Los_Angeles",
        "type": "appliance",
        "id": "123457"
    }
]
</code></pre>


# Admins #
Contains the List, Create, Delete, and Update Admin API calls.
## admins.List ##
Return a list of admins for an organization ID.
    
<pre><code>Arguments:

    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object</code></pre>

### Example: ###
<pre><code>burpy.admins.List(key=mykey, org_id=myorg_id)</code></pre>

## admins.Create ##
Creates a new admin in the Meraki® dashboard.
    
<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: name - The name of the dashboard administrator.
    type arg3: str
    
    param arg4: email address - The email of the dashboard administrator. This attribute can not be updated.
    type arg4: str
    
    param arg5: orgAccess - The privilege of the dashboard administrator on the organization. (i.e.: none, read-only, full)
    type arg5: str
    
    param arg6 (optional): tags - List with a nested dictionary of tags and access for that that tag for the dashboard admin.
    type arg6: list with nested dict
    usage: [{"tag':"<tag_name>", "access":"<permission>"}]
    example: [{"tag":"west", "access":"read-only"}]
    
    param arg7 (optional): networks - List with a nested dictionary of networks and access to that network for the dashboard admin.
    type arg7: list with nested dict
    usage: [{"id":"<net_id>", "access":"<permission>"}]
    example: [{"id":"N_123", "access":"read-only"}, {"id": "N_222", "access":"guest-ambassador"}]</code></pre>

### Example: ###
<pre><code>burpy.admins.Create(key=mykey, org_id=myorg_id, name='Nick Smith', email='nsmith@mydomain.com', org_access='none', tags={'dev':'read-only'}, networks={'N_123':'read-only'})</code></pre>

## admins.Delete ##
Deletes an admin from the Meraki® dashboard.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object 
    
    param arg3: admin_id - Administrator's ID
    type arg3: str</code></pre>

### Example: ###
<pre><code>burpy.admins.Delete(key=mykey, org_id=myorg_id, admin_id='430204')</code></pre>

## admins.Update ##
Updates an existing Meraki® dashboard admin.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: admin_id - Administrator ID.
    type arg3: str
    
    param arg4 (optional): name - The name of the dashboard administrator.
    type arg4: str
    
    param arg5 (optional): org_access - The privilege of the dashboard administrator on the organization. (i.e.: none, read-only, full)
    type arg5: str
    
    param arg6 (optional): tags - List with a nested dictionary of tags and access for that that tag for the dashboard admin.
    type arg6: list with nested dict
    usage: [{"tag':"<tag_name>", "access":"<permission>"}]
    example: [{"tag":"west", "access":"read-only"}]
    
    param arg7 (optional): networks - List with a nested dictionary of networks and access to that network for the dashboard admin.
    type arg7: listh with nested dict
    usage: [{"id":"<net_id>", "access":"<permission>"}]
    example: [{"id":"N_123", "access":"read-only"}, {"id": "N_222", "access":"guest-ambassador"}]</code></pre>

### Example: ###
<pre><code>burpy.admins.Update(key=mykey, org_id=myorg_id, admin_id='1234, name='Nick Doe', org_access='full')</code></pre>

# Cfgtmls #
Configuration template API calls.

## cfgtmls.List ##
List all the configuration templates in the specified organization.
    
<pre><code>Arguments:
    
    param arg1: key - API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.Key object</code></pre>

### Example: ###
<pre><code>burpy.cfgtmls.List(key=mykey, org_id=myorg_id)</code></pre>

## cfgtmls.Remove ##
Remove the specified configuration template in an organization.
    
<pre><code>Arguments:
    
    param arg1: key - API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.Key object
    
    param arg3: tml_id - Template ID.
    type arg3: str</code></pre>

### Example: ###
<pre><code>burpy.cfgtmls.Remove(key=mykey, org_id=myorg_id, tml_id='XXX')</code></pre>

# Clients #
Client API calls.

## clients.List ##
List the clients of a device, up to a maximum of a month ago. 
The usage of each client is returned in kilobytes. If the device is a switch, 
the switchport is returned; otherwise the switchport field is null.
    
<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.Session.Key object
    
    param arg2: sn - The device serial number.
    type arg2: str
    
    param arg3: timespan - The timespan of the data that is returned. 
                           Max timespan is 1 month in seconds.
    type arg3: int</code></pre>

### Example: ###
<pre><code>burpy.clients.List(key=mykey, sn='XQT798XXX', timespan=600)</code></pre>

# Devices #
Device API calls.

## devices.List ##
List all the devices in the specified network.
    
<pre><code>Arguments:
    
    param arg1: key - API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str</code></pre>

### Example: ###
<pre><code>burpy.devices.List(key=mykey, net_id='N_123')</code></pre>

## devices.Return ##
Return lists the device specified by the serial number.
    
<pre><code>Arguments:
    
    param arg1: key - API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: sn - Serial Number.
    type arg3: str</code></pre>

### Example: ###
<pre><code>burpy.devices.Return(key=mykey, net_id='N_123', sn='xj4390rxo')</code></pre>

## devices.Claim ##
Claim a device for your network.
    
<pre><code>Arguments:
    
    param arg1: key - API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: sn - Device serial number.
    type arg3: str</code></pre>

### Example: ###
<pre><code>burpy.devices.Claim(key=mykey, net_id='N_123', sn='xj4390rxo')</code></pre>

## devices.Remove ##
Claim a device for your network.
    
<pre><code>Arguments:
    
    param arg1: key - API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: sn - Device serial number.
    type arg3: str</code></pre>

### Example: ###
<pre><code>burpy.devices.Remove(key=mykey, net_id='N_123', sn='xj439rxo')</code></pre>

## devices.Update ##
Update, updates either the name, subnet or appliance IP.
    
<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: sn - Serial Number of the device.
    type arg3: str
    
    param arg4(optional): name - Name of the device.
    type arg4: str
    
    param arg5(optional): lat - Location, latitude of the device.
    type arg5: float
    
    param arg6(optional): lng - Location, longitude of the device.
    type arg6: float
    
    param arg7(optional): tags - Tags you want to associate with the device.
    type arg7: list</code></pre>

### Example: ###
<pre><code>burpy.devices.Update(key=mykey, net_id='N_123', sn='xj439rxo', name='myname', lat=90, lng=-180, tags=['dev', 'int'])</code></pre>


# Networks #
Network API calls.

## networks.List ##
Returns a list of networks for an organization ID.
    
<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object</code></pre>

### Example: ###
<pre><code>burpy.networks.List(key=mykey, org_id=myorg_id)</code></pre>

## networks.Return ##
Return returns the network specified with the net_id argument.
    
<pre><code>Arguments:
    
    param arg1: key - API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str</code></pre>

### Example: ###
<pre><code>burpy.networks.Return(key=mykey, net_id='N_123')</code></pre>

## networks.Create ##
Creates a new network in an organization. 
    
<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: name - The name of the new network.
    type arg3: str
    
    param arg4: net_type - The type of the new network. 
    Valid types are 'wireless' (for MR), 'switch' (for MS), and 'appliance' (for MX or Z1).
    type arg4: str
    
    param arg5 (optional): tags - Tags to assoicate with the network. This is a list tags should be
    added like the following: tags=['dev', 'int']
    type arg5: list
    
    param arg6 (optional): time_zone - Specify the timezone of the network.
    For a list of allowed timezones, please see the 'TZ' column in the table in this
    https://en.wikipedia.org/wiki/List_of_tz_database_time_zones article. CASE-SENSITIVE!!!
    type arg6: str</code></pre>

### Example: ###
<pre><code>burpy.networks.Create(key=mykey, org_id=myorg_id, name='TestNet', net_type='appliance', tags=['dev', 'int'], time_zone='America/Los_Angeles')</code></pre>

## networks.Update ##
Updates a new network's name, tags or timezone in an organization. 

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: net_id - Network ID.
    type arg3: str
    
    param arg4 (optional): name - The name of the new network.
    type arg4: str
    
    param arg5 (optional): tags - Tags to assoicate with the network. This is a list tags should be
    added like the following: tags=['dev', 'int']
    type arg5: list
    
    param arg6 (optional): time_zone - Specify the timezone of the network.
    For a list of allowed timezones, please see the 'TZ' column in the table in this
    https://en.wikipedia.org/wiki/List_of_tz_database_time_zones article. CASE-SENSITIVE!!!
    type arg6: str</code></pre>

### Example: ###
<pre><code>burpy.networks.Update(key=mykey, org_id=myorg_id, net_id='N_1234', name='ProdNet', tags=['prod'], time_zone='America/New_York')</code></pre>

## networks.Bind ##
Bind a network to a template.

<pre><code>Arguments:
    
    param arg1: key - API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: cfgtml_id - Configuration Template ID.
    type arg3: str
    
    param arg4(optional): auto_bind - Auto bind a network to a template.
    Optional boolean indicating whether the network's switches should automatically 
    bind to profiles of the same model. Defaults to false if left unspecified. This 
    option only affects switch networks and switch templates. Auto-bind is not valid 
    unless the switch template has at least one profile and has at most one profile 
    per switch model.
    type arg4: boolean</code></pre>

### Example: ###
<pre><code>burpy.networks.Bind(key=mykey, org_id=myorg_id, net_id='N_1234', cfgtml_id='N_98765')</code></pre>

## networks.Unbind ##
The Unbind function unbinds a network from a template.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str</code></pre>

### Example: ###
<pre><code>burpy.networks.Unbind(key=mykey, net_id='N_98765')</code></pre>

## networks.Delete ##
Deletes a network from an organziation.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str</code></pre>

### Example: ###
<pre><code>burpy.networks.Delete(key=mykey, net_id='N_1234')</code></pre>

## networks.TrafficAnalysis ##
TrafficAnalysis returns traffic analysis information about the specified network.
Traffic Analysis with Hostname Visibility must be enabled on the network.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: timespan - The timespan for the data. Must be an integer 
    representing a duration in seconds between two hours and one month.
    type arg3: int
    
    param arg4 (optional): device_type - Filter the data by device type: 
    combined (default), wireless, switch, appliance. 
    When using combined, for each rule the data will come from the device 
    type with the most usage.
    type arg4: str</code></pre>

### Example: ###
<pre><code>burpy.networks.TrafficAnalysis(key=mykey, net_id='N_1234', timespan=7200)</code></pre>

## networks.ReturnVPN ##
ReturnVPN returns the site-to-site VPN config of the specified network.
**Only valid for MX networks**

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str</code></pre>

### Example: ###
<pre><code>burpy.networks.ReturnVPN(key=mykey, net_id='N_1234')</code></pre>

## networks.UpdateVPN ##
UpdateVPN updates the site-to-site VPN config of the specified network.  
**Only valid for MX networks**  
*This module hasn't been tested*

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3(optional): mode - The site-to-site VPN mode: hub, spoke or none.
    type arg3: str
    
    param arg4:(optional): hubs - The list of VPN hubs, in order of preference. 
                                  In spoke mode, at least 1 hub is required.
                           * hubId: The network ID of the hub.
                           * useDefaultRoute: Only valid in 'spoke' mode. Indicates whether default route traffic should be sent to this hub.
    type arg4: list with nested dict
    usage: [{"hubId": <net_id>, "useDefaultRoute": <boolean>}]
    example: [{"hubId":"N_1234","useDefaultRoute":True },{"hubId":"N_2345","useDefaultRoute":False}]
    param arg5(optional): subnets - Only applicable in split mode. The list of subnets and their VPN presence.
                          * localSubnet: The CIDR notation subnet used within the VPN
                          * useVpn: Indicates the presence of the subnet in the VPN
    type arg5: list with nested dict
    usage: [{"localSubnet": "<subnet>", "useVpn" :<boolean>}]
    example: [{"localSubnet": "192.168.1.0/24", "useVpn":True}, {"localSubnet": "192.168.128.0/24", "useVpn":False}]</code></pre>


# Orgs #
Organization API calls.

## orgs.List ##
List all the organizations associated with an admin.

<pre><code>Arguments:

    param arg1: key - API key.
    type arg1: str or burpy.session.Key object</code></pre>

### Example: ###
<pre><code>burpy.orgs.List(key=mykey)</code></pre>

## orgs.Return ##
Returns the organization information.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object</code></pre>

### Example: ###
<pre><code>burpy.orgs.Return(key=mykey, org_id=myorg_id)</code></pre>

## orgs.Create ##
Create a new organziation.

<pre><code>Arguments:
    
    param arg1: key - API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: name - The name of the new organization.
    type arg2: str</code></pre>

### Example: ###
<pre><code>burpy.orgs.Create(key=mykey, name='My_New_Org')</code></pre>

## orgs.Update ##
Update an organization's name.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: name - The new name of the organization.
    type arg3: str</code></pre>

### Example: ###
<pre><code>burpy.orgs.Update(key=mykey, org_id=myorg_id, name='new_org_name')</code></pre>

## orgs.Clone ##
Clone an organization to a new organization.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Existing organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: name - The name of the new organization.
    type arg3: str</code></pre>

### Example: ###
<pre><code>burpy.orgs.Clone(key=mykey, org_id='existing_org_id', name='new_org_name')</code></pre>

## orgs.Claim ##
Claim a device, license key, or order into an organization. 
When claiming by order, all devices and licenses in the order will be claimed; 
licenses will be added to the organization and devices will be placed in the 
organization's inventory. These three types of claims are mutually exclusive 
and cannot be performed in one request.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3(optional): order - The order number that should be claimed.
    type arg3: str
    
    param arg4(optional): sn - The serial number of the device that should be claimed.
    type arg4: str
    
    param arg5(optional): license_key - The license key that should be claimed.
    type arg5: str
    
    param arg6(optional): license_mode - Either "renew" or "addDevices". "addDevices" will 
    increase the license limit, while "renew" will extend the amount of time until 
    expiration. This parameter is required when claiming by licenseKey.
    type arg6: str</code></pre>

### Example: ###
<pre><code>burpy.orgs.Claim(key=mykey, org_id=myorg_id, license_key='Q4F343JK', license_mode='renew')</code></pre>

## orgs.ReturnInv ##
Returns the inventory for an organization.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object</code></pre>

### Example: ###
<pre><code>burpy.orgs.ReturnInv(key=mykey, org_id=myorg_id)</code></pre>

## orgs.Return3rdVPN ##
Return third party VPN peers for the organization.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object</code></pre>

### Example: ###
<pre><code>burpy.orgs.Return3rdVPN(key=mykey, org_id=myorg_id)</code></pre>

## orgs.ReturnLic ##
Returns the licenses for an organization.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object</code></pre>

### Example: ###
<pre><code>burpy.orgs.ReturnLic(key=mykey, org_id=myorg_id)</code></pre>

## orgs.ReturnSNMP ##
Returns the SNMP settings for an organization.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object</code></pre>

### Example: ###
<pre><code>burpy.orgs.ReturnSNMP(key=mykey, org_id=myorg_id)</code></pre>

## orgs.Update3rdVPN ##
Update the third party VPN peers for the organization.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: name - Name of the VPN peer.
    type arg3: str
    
    param arg4: public_ip - The public IP of the VPN peer.
    type arg4: str
    
    param arg5: priv_sub - The list of private subnets of the VPN peer.
    type arg5: lst
    
    param arg6: secret - The shared secret with the VPN peer.
    type arg6: str</code></pre>

### Example: ###
<pre><code>burpy.orgs.Update3rdVPN(key=mykey, org_id=myorg_id, name='NewVPN', public_ip='10.100.100.100', priv_sub=['192.168.1.0/24', '192.168.2.0/24'], secret='mysecret')</code></pre>

## orgs.UpdateSNMP ##
UpdateSNMP updates the organizations SNMP settings.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: v2c - SNMP v2c setting.
    type arg3: bool
    
    param arg4: v3 - SNMP v3 setting.
    type arg4: bool
    
    param arg5: v3auth - V3 authentication method.
    type arg5: str (Only accpets 'MD5' or 'SHA')
    
    param arg6: v3auth_pass - V3 authentication password.
    type arg6: str (Must be 8+ characters)
    
    param arg7: v3priv - The SNMP version 3 privacy mode DES or AES128
    type arg7: str
    
    param arg8: v3priv_pass - The SNMP version 3 privacy password. Must be at least 8 characters if specified.
    type arg8: str
    
    param arg9: alwd_ips - The IPs (IPv4) that are allowed to access the SNMP server.
    type arg9: lst</code></pre>

### Example: ###
<pre><code>burpy.orgs.UpdateSNMP(key=mykey, org_id=myorg_id, v3=True, v3auth='SHA', v3auth_pass='MYpassword89', v3priv='AES128', v3priv_pass='myOtherpwd32', alwd_ips=['192.168.100.4', '192.168.100.5'])</code></pre>


# SAML #
Security Assertion Markup Language (SAML) API calls.

## saml.List ##
List the SAML roles for an organization ID.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object</code></pre>

### Example: ###
<pre><code>burpy.saml.List(key=mykey, org_id=myorg_id)</code></pre>

## saml.Remove ##
Removes a SAML role from an organization ID.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: saml_id - SAML role ID.
    type arg3: str</code></pre>

### Example: ###
<pre><code>burpy.saml.Remove(key=mykey, org_id=myorg_id)</code></pre>

# SSIDs #
SSID API calls.

## ssids.List ##
Lists the SSIDs for the specified network.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str</code></pre>

### Example: ###
<pre><code>burpy.ssids.List(key=mykey, net_id='N_1234')</code></pre>

## ssids.Return ##
Returns the specified SSID.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: ssid_num - Service Set Identifier number.
    type arg3: int</code></pre>

### Example: ###

<pre><code>burpy.ssids.Return(key=mykey, net_id='N_1234', ssid_num=4)</code></pre>

## ssids.Update ##
Updates the specified SSID.

<pre><code>Arguments:

    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: ssid_num - Service Set Identifier.
    type arg3: int

    param arg4: name - The name of the SSID.
    type arg4: str

    param arg5: enabled - Enables or disables the SSID.
    type arg5: boolean

    param arg6: auth_mode - Authentication mode only 'open' or 'psk' is accepted.
    type arg6: str ('open' or 'psk')

    param arg7: encrypt_mode - Encryption mode only accepts 'wpa' or 'wep' and
                and auth_mode is set to 'psk'.
    type arg7: str ('wpa' or 'wep')

    param arg8: psk - Pre-shared key.
    type arg8: str</code></pre>

### Example: ###

<pre><code>burpy.ssids.Update(key=mykey, net_id= "N_1234", ssid_num=4, name='Test_SSID', enabled=False, auth_mode='psk', encrypt_mode='wpa', psk='789afdegrahra')</code></pre>

# VLANs #
VLAN API calls.

## vlans.List ##
Lists the VLANs in the specified network.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str</code></pre>

### Example: ###
<pre><code>burpy.vlans.List(key=mykey, net_id='N_1234')</code></pre>

## vlans.Return ##
Returns information from the specified VLAN.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: vlan_id - VLAN ID.
    type arg3: int</code></pre>

### Example: ###
<pre><code>burpy.vlans.Return(key=mykey, net_id='N_1234', vlan_id=20)</code></pre>

## vlans.Add ##
Adds a VLAN to the specified network.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: vlan_id - VLAN ID.
    type arg3: int
    
    param arg4: name - Name of VLAN.
    type arg4: str
    
    param arg5: subnet - The subnet of the VLAN.
    type arg5: str
    example: "192.168.10.0/24"
    
    param arg6: appliance_ip - Appliance IP address.
    type arg6: str</code></pre>

### Example: ###
<pre><code>burpy.vlans.Add(key=mykey, net_id='N_1234', vlan_id=10, name='vlan_10', subnet='192.168.100.0/24', appliance_ip='192.168.100.1')</code></pre>

## vlans.Update ##
Updates either the name, subnet or appliance IP.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: vlan_id - VLAN ID cannot be updated.
    type arg3: int
    
    param arg4(optional): name - Name of VLAN.
    type arg4: str
    
    param arg5(optional): subnet - The subnet of the VLAN.
    type arg5: str
    
    param arg6(optional): appliance_ip - Appliance IP address.
    type arg6: str</code></pre>

### Example: ###
<pre><code>burpy.vlans.Update(key=mykey, net_id='N_1234', vlan_id=10, name='vlan_ten', subnet='192.168.101.0/24', appliance_ip='192.168.101.2')</code></pre>

## vlans.Delete ##
Deletes the specified VLAN from a network.

<pre><code>Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: vlan_id - VLAN ID.
    type arg3: int</code></pre>

### Example: ###
<pre><code>burpy.vlans.Delete(key=mykey, net_id='N_1234', vlan_id=10)</code></pre>