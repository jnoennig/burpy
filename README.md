Burpy
=====

burpy 0.9.a1  
Released: 05-Oct-2016

Burpy is a Python package that utilizes the Meraki® Provisioning REST API. Burpy has
no affiliation with Meraki®.

## Release Notes ##
* 0.9
    * Date: 21-Sep-2016
    * Description: Initial Release
* 0.9.a1
    * Date: 05-Oct-2016
    * Description: Added the ssids package to Burpy.

## Requirements ##
* Python 2.7.x
* Python "Requests" package (tested with: 2.11.1)
    * How to install requests: *pip install requests*

## Brief How-To: ##
Once you install and import burpy the first thing you will need to do is create 
a variable for your API key. All the burpy modules require the API key as a parameter.

This can be done by creating a variable and copying and pasting your key or you can 
use the [session](./EXAMPLES.md) module to read your key from a file.

The modules in the packages will either return a http status code or http status code 
and json data in a tuple. Remove and Delete will return only the http status code 204, 
if successful. Most other modules will return the tuple (http_status_code, json_data).
If you want to print the data in a human readable format use the burpy.pjson() function. 
Details can be found in the [EXAMPLES.md](./EXAMPLES.md) file.
<pre><code>
Example:
burpy.networks.List(key=mykey, org_id=myorg)

Return:
(200, [{u'name': u'Test_Network', u'tags': u' dev int ', u'organizationId': u'555555', 
u'timeZone': u'America/Detroit', u'type': u'appliance', u'id': u'N_123456'}, 
{u'name': u'Network2', u'tags': u'', u'organizationId': u'555555', u'timeZone': 
u'America/New_York', u'type': u'appliance', u'id': u'N_123457'}])
</code></pre>

## Modules & Packages in burpy ##
<pre><code>Hirearchy

|__burpy (package)
|____session
|____pjson
|____admins (package)
| |____Create
| |____Delete
| |____List
| |____Update
|____cfgtmls (package)
| |____List
| |____Remove
|____clients (package)
| |____List
|____devices (package)
| |____Claim
| |____List
| |____Remove
| |____Return
| |____Update
|____networks (package)
| |____Bind
| |____Create
| |____Delete
| |____List
| |____Return
| |____ReturnVPN
| |____TrafficAnalysis
| |____Unbind
| |____Update
| |____UpdateVPN
|____orgs (package)
| |____Claim
| |____Clone
| |____Create
| |____List
| |____Return
| |____Return3rdVPN
| |____ReturnInv
| |____ReturnLicense
| |____ReturnSNMP
| |____Update
| |____Update3rdVPN
| |____UpdateSNMP
|____saml (package)
| |____List
| |____Remove
| |____Return
|____ssids (package)
| |____List
| |____Return
| |____Update
|____vlans (package)
| |____Add
| |____Delete
| |____List
| |____Return
| |____Update
</code></pre>

>All product and company names are trademarks™ or registered® trademarks of 
>their respective holders. Use of them does not imply any affiliation with 
>or endorsement by them. 