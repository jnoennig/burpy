#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import re
import requests
from burpy import my_headers


def _val_name(name):
    """
    _val_name validates the name.
    """
    if bool(name) is True:
        return {'name': name}
    else:
        return ''


def _val_org_access(org_access):
    """
    _val_org_access validates the org_access.
    """
    if bool(org_access) is True:
        org_access = org_access.lower()
        regex_org_access = re.compile('none|read[-]only|full')
        org_access_match = regex_org_access.search(org_access)
        if org_access_match:
            return {'orgAccess': org_access}
        else:
            print 'Verify org_access is one of the following: none|read-only|full.'
            return 1
    else:
        return ''
        

def _val_tags(tags):
    """
    _val_tags validates the tags.
    """
    if bool(tags) is True:
        if bool(tags[0]) is True:
            tag_lst = []
            regex_tag_access = re.compile('guest[-]ambassador|monitor[-]only|read[-]only|full')
            for tag in tags:
                tag['access'] = tag['access'].lower()
                match_tag_access = regex_tag_access.search(tag['access'])
                if match_tag_access:
                    tag_lst.append(tag)
                else:
                    print "Verify you used 'guest-ambassador|monitor-only|read-only|full' as your permissions."
                    return 1
            return {'tags': tag_lst}
        else:
            return {'tags': []}
    else:
        return ''
            
        
def _val_networks(networks):
    """
    _val_networks validates the networks.
    """
    if bool(networks) is True:
        if bool(networks[0]) is True:
            net_lst = []
            regex_net_access = re.compile('guest[-]ambassador|monitor[-]only|read[-]only|full')
            for net in networks:
                net['access'] = net['access'].lower()
                match_net_access = regex_net_access.search(net['access'])
                if match_net_access:
                    net_lst.append(net)
                else:
                    print "Verify you used 'guest-ambassador|monitor-only|read-only|full' as your permissions."
                    return 1
            return {'networks': net_lst}
        else:
            return {'networks': []}
    else:
        return ''


def _data_join(name, org_access, tags, networks):
    """
    _data_join joins name, org_access, tags, and networks into one string.
    """
    name = _val_name(name)
    org_access = _val_org_access(org_access)
    tags = _val_tags(tags)
    networks = _val_networks(networks)
    payload = {}
    if bool(name) is True or bool(org_access) is True or bool(tags) is True or bool(networks) is True:
        if bool(name) is True:
            payload.update(name)
        if bool(org_access) is True:
            payload.update(org_access)
        if bool(tags) is True:
            payload.update(tags)
        if bool(networks) is True:
            payload.update(networks)
        return payload
    else:
        print "Specify at least one parameter to update."
        return 1


def Update(key='', org_id='', admin_id='', name='', org_access='', tags=[], networks=[]):
    """Updates an existing admin in the Meraki dashboard.
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: admin_id - Administrator ID.
    type arg3: str
    
    param arg4 (optional): name - The name of the dashboard administrator.
    type arg4: str
    
    param arg5 (optional): org_access - The privilege of the dashboard administrator on the organization.
    (i.e.: none, read-only, full)
    type arg5: str
    
    param arg6 (optional): tags - List with a nested dictionary of tags and access for that that tag
    for the dashboard admin.
    type arg6: list with nested dict
    usage: [{"tag':"<tag_name>", "access":"<permission>"}]
    example: [{"tag":"west", "access":"read-only"}]
    
    param arg7 (optional): networks - List with a nested dictionary of networks and access to that
    network for the dashboard admin.
    type arg7: listh with nested dict
    usage: [{"id":"<net_id>", "access":"<permission>"}]
    example: [{"id":"N_123", "access":"read-only"}, {"id": "N_222", "access":"guest-ambassador"}]
    """
    try:
        key = str(key)
        org_id = str(org_id)
        payload = json.dumps(_data_join(name, org_access, tags, networks))
        uri = 'https://dashboard.meraki.com/api/v0/organizations/' + org_id + '/admins/' + admin_id
        r = requests.put(uri, data=payload, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
        return 1
