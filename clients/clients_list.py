#!/usr/bin/env python
#encoding:utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import requests
from burpy import my_headers

def List(key='', sn='', timespan=0):
    """List the clients of a device, up to a maximum of a month ago.
    The usage of each client is returned in kilobytes. If the device is a switch,
    the switchport is returned; otherwise the switchport field is null.
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.Session.Key object
    
    param arg2: sn - The device serial number.
    type arg2: str
    
    param arg3: timespan - The timespan of the data that is returned.
                           Max timespan is 1 month in seconds.
    type arg3: int
    """
    try:
        if timespan > 0 and timespan <= 2592000:
            timespan = str(timespan)
            key = str(key)
            uri = 'https://n132.meraki.com/api/v0/devices/' + sn + '/clients?timespan=' + timespan
            r = requests.get(uri, headers=my_headers.headers(key))
            json_output = r.json()
            return json_output
        else:
            print "[Error] Timespan needs to be more than 0 and less than 2592000 seconds."
            return 1
    except Exception, error:
        print error
        return 1
