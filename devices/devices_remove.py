#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import requests
from burpy import my_headers

def Remove(key='', net_id='', sn=''):
    """Remove a device from your network.
    
    Arguments:
    
    param arg1: key - API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: sn - Device serial number.
    type arg3: str
    """
    try:
        key = str(key)
        uri = "https://dashboard.meraki.com/api/v0/networks/" + net_id + "/devices/" + sn + "/remove"
        r = requests.post(uri, headers=my_headers.headers(key))
        return r.status_code
    except Exception, error:
        print error
        return 1
