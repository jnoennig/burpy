#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import requests
from burpy import my_headers


def _val_name(name):
    """
    _val_name validates the name.
    """
    if bool(name) is True:
        name = {"name": name}
        return name
    else:
        return ''

def _val_lat(lat):
    """
    _val_lat validates the latitude.
    """
    if lat <= 90.0 and lat >= -90.0:
        lat = str(lat)
        lat = {"lat": lat}
        return lat
    else:
        return ''
    

def _val_lng(lng):
    """
    _val_lng validates the longitude.
    """
    if lng <= 180.0 and lng >=-180.0:
        lng = str(lng)
        lng = {"lng": lng}
        return lng
    else:
        return ''
    

def _val_tags(tags):
    """
    _val_tags validates the tags.
    """
    if bool(tags) is True:
        if isinstance(tags, list):
            tag_str =  ' '.join(tags)
            tag_str = {"tags": tag_str}
            return tag_str
        else:
            print 'The tags parameter needs to be a list.'
            return
    else:
        return ''


def _data_join(name, lat, lng, tags):
    """
    _data_join joins name, lat, lng, and tags into one string.
    """
    name = _val_name(name)
    lat = _val_lat(lat)
    lng = _val_lng(lng)
    tags = _val_tags(tags)
    payload = {}
    if bool(name) is True or bool(lat) is True or bool(lng) is True or bool(tags) is True:
        if bool(name) is True:
            payload.update(name)
        if bool(lat) is True:
            payload.update(lat)
        if bool(lng) is True:
            payload.update(lng)
        if bool(tags) is True:
            payload.update(tags)
        return payload
    else:
        print 'At least one parameter needs to be updated: name, lat, lng or tags.'


def Update(key='', net_id='', sn='', name='', lat=90.1, lng=180.1, tags=None):
    """Update, updates either the name, subnet or appliance IP.
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: sn - Serial Number of the device.
    type arg3: str
    
    param arg4(optional): name - Name of the device.
    type arg4: str
    
    param arg5(optional): lat - Location, latitude of the device.
    type arg5: float
    
    param arg6(optional): lng - Location, longitude of the device.
    type arg6: float
    
    param arg7(optional): tags - Tags you want to associate with the device.
    type arg7: list
    """
    try:
        key = str(key)
        payload = json.dumps(_data_join(name, lat, lng, tags))
        uri = "https://dashboard.meraki.com/api/v0/networks/" + net_id + "/devices/" + sn
        r = requests.put(uri, data=payload, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
        return 1
