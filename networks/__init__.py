#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

from burpy.networks.networks_list import List
from burpy.networks.networks_return import Return
from burpy.networks.networks_create import Create
from burpy.networks.networks_delete import Delete
from burpy.networks.networks_update import Update
from burpy.networks.networks_return_vpn import ReturnVPN
from burpy.networks.networks_traffic import TrafficAnalysis
from burpy.networks.networks_update_vpn import UpdateVPN
from burpy.networks.networks_bind import Bind
from burpy.networks.networks_unbind import Unbind
