#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import requests
from burpy import my_headers


def _val_cfgtml_id(cfgtml_id):
    """
    _val_cfgtml_id validates the cfgtml_id.
    """
    if bool(cfgtml_id):
        return {"configTemplateId": cfgtml_id}
    else:
        print "'cfgtml_id' needs to be specified."
        return 1
    

def _val_autoBind(auto_bind):
    """
    _val_autoBind validates auto_bind.
    """
    if auto_bind != None:
        if auto_bind is True or auto_bind is False:
            return {"autoBind": auto_bind}
        else:
            print "Verify auto_bind is True or False."
    else:
        return ''


def _data_join(cfgtml_id, auto_bind):
    """
    _data_join joins cfgtml_id and auto_bind into one string.
    """
    cfgtml_id = _val_cfgtml_id(cfgtml_id)
    auto_bind = _val_autoBind(auto_bind)
    payload = {}
    if bool(cfgtml_id) is True:
        payload.update(cfgtml_id)
        if bool(auto_bind) is True:
            payload.update(auto_bind)
        return payload
    else:
        print "'cfgtml_id' must be specified.'"
        return 1


def Bind(key='', net_id='', cfgtml_id='', auto_bind=None):
    """Bind a network to a template.
    
    Arguments:
    
    param arg1: key - API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: cfgtml_id - Configuration Template ID.
    type arg3: str
    
    param arg4(optional): auto_bind - Auto bind a network to a template.
Optional boolean indicating whether the network's switches should automatically
bind to profiles of the same model. Defaults to false if left unspecified. This
option only affects switch networks and switch templates. Auto-bind is not valid
unless the switch template has at least one profile and has at most one profile
per switch model.
    type arg4: boolean
    """
    try:
        key = str(key)
        payload = json.dumps(_data_join(cfgtml_id, auto_bind))
        uri = "https://dashboard.meraki.com/api/v0/networks/" + net_id + "/bind"
        r = requests.post(uri, data=payload, headers=my_headers.headers(key))
        return r.status_code
    except Exception, error:
        print error
        return 1
