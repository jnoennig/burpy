#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import re
import requests
from burpy import my_headers
from burpy import timezones


def _val_name(name):
    """
    _val_name validates the name.
    """
    if bool(name) == True:
        name = {'name': name}
        return name
    else:
        print 'A name is required when creating a network.'
        return 1


def _val_net_type(net_type):
    """
    _val_net_type validates the network type is legitimate.
    """
    if bool(net_type) is True:
        regex_net_type = re.compile('wireless|switch|appliance')
        net_type = net_type.lower()
        match_net_type = regex_net_type.search(net_type)
        if match_net_type:
            net_type = {'type': net_type}
            return net_type
        else:
            print "Verify you have used 'wireless', 'switch', or 'appliance' as your network type."
            return 1
    else:
        print '''A network type is required when creating a network.
Valid Network Types: 'wireless', 'switch', 'appliance'.'''

def _frm_tags(tags):
    """
    _frm_tags puts the tags in the correct format for the API call.
    """
    if bool(tags) is True:
        if isinstance(tags, list):
            tag_str =  ' '.join(tags)
            tag_dict = {'tags': tag_str}
            return tag_dict
        else:
            print 'The tags parameter needs to be a list.'
            return 1
    else:
        return ''
    
def _val_timezone(timezone):
    """
    _val_timezone validates the inputted timezone is legitimate. CASE-SENSITIVE
    """
    if bool(timezone) is True:
        timezone = timezone
        timezones_lst = timezones.tz_lst()
        if timezone in timezones_lst:
            return {'timeZone': timezone}
        else:
            print 'Please enter a valid timezone.'
    else:
        return ''
  

def _data_join(name, net_type, tags, timezone):
    """
    _data_join joins the parameters in the correct format for the HTTP request.
    """
    name = _val_name(name)
    net_type = _val_net_type(net_type)
    tags = _frm_tags(tags)
    timezone = _val_timezone(timezone)
    payload = {}
    if bool(name) is True and bool(net_type) is True:
        payload.update(name)
        payload.update(net_type)
        if bool(tags) is True:
            payload.update(tags)
        if bool(timezone) is True:
            payload.update(timezone)
        return payload
    else:
        print 'Name and network type parameters need to be specified.'
        return 1


def Create(key='', org_id='', name='', net_type='', tags=None, timezone=''):
    """Creates a new network in an organization.
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: name - The name of the new network.
    type arg3: str
    
    param arg4: net_type - The type of the new network.
    Valid types are 'wireless' (for MR), 'switch' (for MS), and 'appliance' (for MX or Z1).
    type arg4: str
    
    param arg5 (optional): tags - Tags to assoicate with the network. This is a list tags should be
    added like the following: tags=['dev', 'int']
    type arg5: list
    
    param arg6 (optional): time_zone - Specify the timezone of the network.
    For a list of allowed timezones, please see the 'TZ' column in the table in this
    https://en.wikipedia.org/wiki/List_of_tz_database_time_zones article. CASE-SENSITIVE!!!
    type arg6: str
    """
    try:
        key = str(key)
        org_id = str(org_id)
        payload = json.dumps(_data_join(name, net_type, tags, timezone))
        uri = "https://dashboard.meraki.com/api/v0/organizations/" + org_id + "/networks"
        r = requests.post(uri, data=payload, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
        return 1
