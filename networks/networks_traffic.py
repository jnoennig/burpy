#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import re
import requests
from burpy import my_headers


def _val_timespan(timespan):
    """
    _val_timespan validates timespan.
    """
    if bool(timespan):
        if timespan >= 7200 and timespan <= 2592000:
            return str(timespan)
        else:
            print 'Timespan needs to be between 7200 and 2592000.'
            return 1
    else:
        print 'Timespan is mandatory.'
        return 1

def _val_device_type(device_type):
    """
    _val_device_type validates device_type.
    """
    regex = re.compile('combined|wireless|switch|appliance')
    if bool(device_type):
        match_regex = regex.search(device_type)
        if match_regex:
            return "?deviceType=" + device_type
        else:
            print 'Verify device type (combined|wireless|switch|appliance).'
            return 1
    else:
        return ''

def _data_join(timespan, device_type):
    """
    _data_join joins timespan and device_type.
    """
    timespan = _val_timespan(timespan)
    device_type = _val_device_type(device_type)
    data_lst = []
    data_lst.append(timespan)
    if bool(device_type):
        data_lst.append(device_type)
    data = ''.join(data_lst)
    return data
    

def TrafficAnalysis(key='', net_id='', timespan=0, device_type=''):
    """TrafficAnalysis returns traffic analysis information about the specified network.
    Traffic Analysis with Hostname Visibility must be enabled on the network.
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: timespan - The timespan for the data. Must be an integer
    representing a duration in seconds between two hours and one month.
    type arg3: int
    
    param arg4 (optional): device_type - Filter the data by device type:
    combined (default), wireless, switch, appliance.
    When using combined, for each rule the data will come from the device
    type with the most usage.
    type arg4: str
    """
    try:
        key = str(key)
        data = _data_join(timespan, device_type)
        uri = "https://n132.meraki.com/api/v0/networks/" + net_id + "/traffic?timespan=" + data
        r = requests.get(uri, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
