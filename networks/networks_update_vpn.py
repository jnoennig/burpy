#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import re
import requests
from burpy import my_headers


def _val_mode(mode):
    """
    _val_mode validates the mode is legitimate.
    """
    if bool(mode) is True:
        regex_mode = re.compile('hub|spoke|none')
        mode = mode.lower()
        match_mode = regex_mode.search(mode)
        if match_mode:
            return {"mode": mode}
        else:
            print "Verify you have used 'hub', 'spoke', or 'none' as your vpn mode."
            return 1
    else:
        return ''

def _val_hubs(hubs):
    """
    _val_hubs validates the hubs.
    """
    if bool(hubs) is True:
        if isinstance(hubs, list):
            return {'hubs': hub_lst}
        else:
            print "hubs needs to be a list."
            return 1
    else:
        return ''


def _val_sub(subnets):
    """
    _val_sub validates the subnets.
    """
    if bool(subnets) is True:
        if isinstance(subnets, list):
            subnet_lst = []
            regex_subnet = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\/\d{1,2}$")
            for subnet in subnets:
                match_sub = regex_subnet.search(subnet['localSubnet'])
                if match_sub is True:
                    if subnet['useVpn'] is True or subnet['useVpn'] is False:
                        subnet_lst.append(subnet)
                    else:
                        print "'useVpn' needs to be a boolean (True or False)."
                        return 1
                else:
                    print "Verify the subnet is in the correct format."
                    return 1
            return {"subnets": subnet_lst}
        else:
            print "subnets needs to be a list."
            return 1
    else:
        return ''


def _data_join(mode, hubs, subnets):
    """
    _data_join joins mode and subnets into a string.
    """
    mode = _val_mode(mode)
    subnets = _val_sub(subnets)
    payload = {}
    if bool(mode) is True:
        if bool(hubs) is True or bool(subnets) is True:
            if bool(mode) is True:
                payload.update(mode)
            if bool(subnets) is True:
                payload.update(subnets)
            return payload
    else:
        print "Verify parameters."
        return 1


def UpdateVPN(key='', net_id='', mode='', hubs=None, subnets=None):
    """UpdateVPN updates the site-to-site VPN config of the specified network.
    ***Only valid for MX networks***
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3(optional): mode - The site-to-site VPN mode: hub, spoke or none.
    type arg3: str
    
    param arg4:(optional): hubs - The list of VPN hubs, in order of preference.
                                  In spoke mode, at least 1 hub is required.
                           * hubId: The network ID of the hub.
                           * useDefaultRoute: Only valid in 'spoke' mode. Indicates
                             whether default route traffic should be sent to this hub.
    type arg4: list with nested dict
    usage: [{"hubId": <net_id>, "useDefaultRoute": <boolean>}]
    example: [{"hubId":"N_1234","useDefaultRoute":True },{"hubId":"N_2345","useDefaultRoute":False}]
    param arg5(optional): subnets - Only applicable in split mode. The list of subnets and their VPN presence.
                          * localSubnet: The CIDR notation subnet used within the VPN
                          * useVpn: Indicates the presence of the subnet in the VPN
    type arg5: list with nested dict
    usage: [{"localSubnet": "<subnet>", "useVpn" :<boolean>}]
    example: [{"localSubnet": "192.168.1.0/24", "useVpn":True}, {"localSubnet": "192.168.128.0/24", "useVpn":False}]
    """
    try:
        key = str(key)
        payload = json.dumps(_data_join(mode, hubs, subnets))
        uri = "https://dashboard.meraki.com/api/v0/networks/" + net_id + "/siteToSiteVpn"
        r = requests.put(uri, data=payload, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
        return 1
    