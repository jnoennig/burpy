#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import re
import requests
from burpy import my_headers


def _val_order(order):
    """
    _val_order validates the order.
    """
    if bool(order) is True:
        return {"order": order}
    else:
        return ''

def _val_sn(sn):
    """
    _val_sn validates the sn.
    """
    if bool(sn) is True:
        return {"serial": sn}
    else:
        return ''

def _val_license_key(license_key):
    """
    _val_license_key validates the license_key.
    """
    if bool(license_key) is True:
        return {"licenseKey": license_key}
    else:
        return ''

def _val_license_mode(license_mode):
    """
    _val_license_mode validates the license_mode.
    """
    license_mode = license_mode.lower()
    regex_renew = re.compile('renew')
    regex_add = re.compile('adddevices')
    if bool(license_mode) is True:
        match_renew = regex_renew.search(license_mode)
        match_add = regex_add.search(license_mode)
        if match_renew:
            return {"licenseMode": 'renew'}
        elif match_add:
            return {"licenseMode": 'addDevices'}
        else:
            print "license_mode needs to be either 'renew' or 'addDevices'"
            return 1
    else:
        return ''

def _data_join(order, sn, license_key, license_mode):
    """
    _data_join joins order, sn, license_key, and license_mode into one string.
    """
    order = _val_order(order)
    sn = _val_sn(sn)
    license_key = _val_license_key(license_key)
    license_mode = _val_license_mode(license_mode)
    payload = {}
    if bool(order) is True and bool(sn) is False and bool(license_key) is False and bool(license_mode) is False:
        payload.update(order)
    elif bool(order) is False and bool(sn) is True and bool(license_key) is False and bool(license_mode) is False:
        payload.update(sn)
    elif bool(order) is False and bool(sn) is False and bool(license_key) is True and bool(license_mode) is True:
        payload.update(license_key)
        payload.update(license_mode)
    else:
        print """One of the three types ('order', 'sn', 'license_key') of claims needs to be specified,
but they are mutually exclusive. 'license_mode' is required when claiming by 'license_key'."""
        return 1
    return payload

def Claim(key='', org_id='', order ='', sn='', license_key='', license_mode=''):
    """Claim a device, license key, or order into an organization.
    When claiming by order, all devices and licenses in the order will be claimed;
    licenses will be added to the organization and devices will be placed in the
    organization's inventory. These three types of claims are mutually exclusive
    and cannot be performed in one request.
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3(optional): order - The order number that should be claimed.
    type arg3: str
    
    param arg4(optional): sn - The serial number of the device that should be claimed.
    type arg4: str
    
    param arg5(optional): license_key - The license key that should be claimed.
    type arg5: str
    
    param arg6(optional): license_mode - Either "renew" or "addDevices". "addDevices" will 
    increase the license limit, while "renew" will extend the amount of time until 
    expiration. This parameter is required when claiming by licenseKey.
    type arg6: str
    """
    try:
        key = str(key)
        org_id = str(org_id)
        payload = json.dumps(_data_join(order, sn, license_key, license_mode))
        uri = "https://dashboard.meraki.com/api/v0/organizations/" + org_id + "/claim"
        r = requests.post(uri, data=payload, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
        return 1
