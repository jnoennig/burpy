#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import requests
from burpy import my_headers

def _val_name(name):
    """
    _val_name validates the name.
    """
    if bool(name) is True:
        return {"name": name}
    else:
        print 'Please specify the organizations name.'
        return 1

def Update(key='', org_id='', name=''):
    """Update an organization's name.
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: name - The new name of the organization.
    type arg3: str
    """
    try:
        key = str(key)
        org_id = str(org_id)
        name = _val_name(name)
        payload = json.dumps(name)
        uri = "https://dashboard.meraki.com/api/v0/organizations/" + org_id
        r = requests.put(uri, data=payload, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
        return 1
