#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import re
import requests
from burpy import my_headers


def _val_name(name):
    """
    _val_name validates the name.
    """
    if bool(name) is True:
        return {"name": name}
    else:
        print "Name is required."
        return 1

def _val_public_ip(public_ip):
    """
    _val_public_ip validates the public_ip.
    """
    if bool(public_ip) is True:
        regex = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")
        match = regex.search(public_ip)
        if match:
            return {"publicIp": public_ip}
        else:
            print "The public IP is not a valid IPv4 address."
            return 1
    else:
        print "Public IP is required."
        return 1
    

def _val_priv_sub(subnets):
    """
    _val_priv_sub validates priv_sub.
    """
    subnet_lst = []
    if bool(subnets) is True:
        if isinstance(subnets, list):
            regex = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}[/]\d{1,2}$")
            for subnet in subnets:
                match = regex.search(subnet)
                if match:
                    subnet_lst.append(subnet)
                else:
                    print "Verify the private subnets have the correct format."
                    return 1
            return {"privateSubnets": subnet_lst}
        else:
            print "priv_sub needs to be a list."
            return 1
    else:
        print "Private subnets are required."
        return 1
    
def _val_secret(secret):
    """
    _val_secret validates secret.
    """
    if bool(secret) is True:
        return {"secret": secret}
    else:
        print "The shared secret is required."
        return 1

def _data_join(name, public_ip, priv_sub, secret):
    """
    _data_join joins name, public_ip, priv_sub, and secret into one string.
    """
    name = _val_name(name)
    public_ip = _val_public_ip(public_ip)
    priv_sub = _val_priv_sub(priv_sub)
    secret = _val_secret(secret)
    payload = {}
    payload.update(name)
    payload.update(public_ip)
    payload.update(priv_sub)
    payload.update(secret)
    return payload


def Update3rdVPN(key='', org_id='', name='', public_ip='', priv_sub=None, secret=''):
    """Update the third party VPN peers for the organization.
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: name - Name of the VPN peer.
    type arg3: str
    
    param arg4: public_ip - The public IP of the VPN peer.
    type arg4: str
    
    param arg5: priv_sub - The list of private subnets of the VPN peer.
    type arg5: lst
    
    param arg6: secret - The shared secret with the VPN peer.
    type arg6: str
    """
    try:
        key = str(key)
        org_id = str(org_id)
        data_join_lst = [_data_join(name, public_ip, priv_sub, secret)]
        payload = json.dumps(data_join_lst)
        uri = 'https://dashboard.meraki.com/api/v0/organizations/' + org_id + '/thirdPartyVPNPeers'
        r = requests.put(uri, data=payload, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
        return 1
