#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import requests
from burpy import my_headers


def _val_v2c(v2c):
    """
    _val_v2c validates v2c.
    """
    if v2c != None:
        if v2c is True or v2c is False:
            return {"v2cEnabled": v2c}
        else:
            print "The value of 'v2c' can only be True or False."
            return 1
    else:
        return ''
        
    
def _val_v3(v3):
    """
    _val_v3 validates v3.
    """
    if v3 != None:
        if v3 is True or v3 is False:
            return {"v3Enabled": v3}
        else:
            print "The value of 'v3' can only be True or False."
            return 1
    else:
        return ''

def _val_v3auth(v3auth):
    """
    _val_v3auth validates v3auth.
    """
    if bool(v3auth) is True:
        v3auth = v3auth.upper()
        if v3auth == 'MD5':
            return {"v3AuthMode": v3auth}
        elif v3auth == 'SHA':
            return {"v3AuthMode": v3auth}
        else:
            print "The value of 'v3auth' can only be 'MD5' or 'SHA'."
            return 1
    else:
        return ''
    

def _val_v3auth_pass(v3auth_pass):
    """
    _val_v3auth_pass validates v3auth_pass.
    """
    if bool(v3auth_pass) is True:
        pass_len = len(v3auth_pass)
        if pass_len > 7:
            return {"v3AuthPass": v3auth_pass}
        else:
            print "The 'v3auth_pass' password must be 8 characters or longer."
            return 1
    else:
        return ''
    

def _val_v3priv(v3priv):
    """
    _val_v3priv validates v3priv.
    """
    if bool(v3priv) is True:
        v3priv = v3priv.upper()
        if v3priv == 'DES':
            return {"v3PrivMode": v3priv}
        elif v3priv == 'AES128':
            return {"v3PrivMode": v3priv}
        else:
            print "The value of 'v3priv' can only be 'DES' or 'AES128'."
            return 1
    else:
        return ''
    

def _val_v3priv_pass(v3priv_pass):
    """
    _val_v3priv_pass validates v3priv_pass.
    """
    if bool(v3priv_pass) is True:
        pass_len = len(v3priv_pass)
        if pass_len > 7:
            return {"v3PrivPass": v3priv_pass}
        else:
            print "The 'v3priv_pass' password must be 8 characters or longer."
            return 1
    else:
        return ''

def _val_alwd_ip(alwd_ips):
    """
    _val_alwd_ip validates alwd_ips.
    """
    if bool(alwd_ips) is True:
        if isinstance(alwd_ips, list):
            alwd_ips = ';'.join(alwd_ips)
            return {"peerIps": alwd_ips}
        else:
            print 'alwd_ips must be a list.'
            return 1
    else:
        return ''
    
def _data_join(v2c, v3, v3auth, v3auth_pass, v3priv, v3priv_pass, alwd_ips):
    """
    _data_join joins the parameters in the correct format for the HTTP request.
    """
    v2c = _val_v2c(v2c)
    v3 = _val_v3(v3)
    v3auth = _val_v3auth(v3auth)
    v3auth_pass = _val_v3auth_pass(v3auth_pass)
    v3priv = _val_v3priv(v3priv)
    v3priv_pass = _val_v3priv_pass(v3priv_pass)
    alwd_ips = _val_alwd_ip(alwd_ips)
    payload = {}
    if bool(v2c) is True or bool(v3) is True or bool(v3auth) is True or bool(v3auth_pass) is True \
    or bool(v3priv) is True or bool(v3priv_pass) is True or bool(alwd_ips) is True:
        if bool(v2c) is True:
            payload.update(v2c)
        if bool(v3) is True:
            payload.update(v3)
        if bool(v3auth) is True:
            payload.update(v3auth)
        if bool(v3auth_pass) is True:
            payload.update(v3auth_pass)
        if bool(v3priv) is True:
            payload.update(v3priv)
        if bool(v3priv_pass) is True:
            payload.update(v3priv_pass)
        if bool(alwd_ips) is True:
            payload.update(alwd_ips)
        return payload
    else:
        print "At least one parameter needs to be updated."
        return 1


def UpdateSNMP(key='', org_id='', v2c=None, v3=None, v3auth='', v3auth_pass='', v3priv='', v3priv_pass='', alwd_ips=None):
    """UpdateSNMP updates the organizations SNMP settings.
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: org_id - Organization ID.
    type arg2: str or burpy.session.OrgId object
    
    param arg3: v2c - SNMP v2c setting.
    type arg3: bool
    
    param arg4: v3 - SNMP v3 setting.
    type arg4: bool
    
    param arg5: v3auth - V3 authentication method.
    type arg5: str (Only accpets 'MD5' or 'SHA')
    
    param arg6: v3auth_pass - V3 authentication password.
    type arg6: str (Must be 8+ characters)
    
    param arg7: v3priv - The SNMP version 3 privacy mode DES or AES128
    type arg7: str
    
    param arg8: v3priv_pass - The SNMP version 3 privacy password. Must be at least 8 characters if specified.
    type arg8: str
    
    param arg9: alwd_ips - The IPs (IPv4) that are allowed to access the SNMP server.
    type arg9: lst
    """
    try:
        key = str(key)
        org_id = str(org_id)
        payload = json.dumps(_data_join(v2c, v3, v3auth, v3auth_pass, v3priv, v3priv_pass, alwd_ips))
        uri = "https://dashboard.meraki.com/api/v0/organizations/" + org_id + "/snmp"
        r = requests.put(uri, data=payload, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
        return 1
