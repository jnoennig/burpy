#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""
import re
import os

class Key(object):
    """Returns the private key of a Meraki user
    from a file.
    
    Arguments:
    
    param arg1: Path to file with key. The file name MUST end with ".txt"
    (not case sensitive). (e.g.: /home/bob/key.txt)
    type arg1: str
    """
    def __init__(self, key):
        self.key = key
        fpath = os.path.isfile(self.key)
        regex_fn = re.compile('.*[.]txt', re.IGNORECASE)
        match_fn = regex_fn.search(self.key)
        if fpath is True:
            if match_fn:
                with open(self.key, "rb") as mykf:
                    self.key = mykf.readline().strip()
            else:
                print 'Verify file extension.'
        else:
            print 'Verify your API key text file path.'
    def __repr__(self):
        return self.key

class OrgId(object):
    """Returns the organization ID.
    
    Arguments:
    
    param arg1: Path to file with organization ID. The file name MUST end with ".txt"
    (not case sensitive). (e.g.: /home/bob/orgId.txt)
    type arg1: str
    """
    def __init__(self, org_id):
        self.org_id = org_id
        fpath = os.path.isfile(self.org_id)
        regex_fn = re.compile('.*[.]txt', re.IGNORECASE)
        match_fn = regex_fn.search(self.org_id)
        if fpath is True:
            if match_fn:
                with open(self.org_id, "rb") as myof:
                    self.org_id = myof.readline().strip()
            else:
                print 'Verify file extension.'
        else:
            print 'Verify your organization ID text file path.'
    def __repr__(self):
        return self.org_id
