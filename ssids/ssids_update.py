#/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import re
import requests
from burpy import my_headers

def _val_ssid_num(ssid_num):
    '''
    _val_ssid_num validates the ssid.
    '''
    if bool(ssid_num) is True:
        if ssid_num > 0:
            return str(ssid_num)
        else:
            print 'The SSID number must be greater than 0.'
    else:
        print 'The SSID number must be specified.'

def _val_name(name):
    '''Validates the name variable.

    Arguments:

    param arg1: name - SSID name
    type arg1: str
    '''
    if bool(name):
        return {"name": name}
    else:
        return ''

def _val_enabled(enabled):
    '''Validates the enabled variable.

    Arguments:

    param arg1: enabled - Enables or disables the SSID.
    type arg1: boolean
    '''
    if enabled is True or enabled is False:
        return {"enabled": enabled}
    else:
        return ''

def _val_auth_mode(auth_mode):
    '''Validates the authentication mode.

    Arguments:

    param arg1: auth_mode - Authentication mode only 'open' or 'psk' is accepted.
    type arg1: str ('open' or 'psk')
    '''
    if bool(auth_mode):
        auth_mode = auth_mode.lower()
        regex = re.compile('open|psk')
        match = regex.search(auth_mode)
        if match:
            return {"authMode": auth_mode}
        else:
            print "If auth_mode is specified it needs to be 'open' or 'psk'."
            return 1
    else:
        return ''

def _val_encrypt_mode(encrypt_mode):
    '''Validates the encryption mode. This parameter is only valid if
    auth_mode is set to 'psk'.

    param arg1: encrypt_mode - Encryption mode only accepts 'wpa' or 'wep' and
                and auth_mode is set to 'psk'.
    type arg1: str ('wpa' or 'wep')
    '''
    if bool(encrypt_mode):
        encrypt_mode = encrypt_mode.lower()
        regex = re.compile('wpa|wep')
        match = regex.search(encrypt_mode)
        if match:
            return {"encryptionMode": encrypt_mode}
        else:
            print "encrypt_mode can be 'wep' or 'wpa'."
    else:
        return ''

def _val_psk(psk):
    '''Validates the pre-shared key.

    param arg1: psk - Pre-shared key.
    type arg1: str
    '''
    if bool(psk):
        return {"psk": psk}
    else:
        return ''

def _data_join(name, enabled, auth_mode, encrypt_mode, psk):
    '''Joins the variables into one dictionary for the request
    payload.
    '''
    name = _val_name(name)
    enabled = _val_enabled(enabled)
    auth_mode = _val_auth_mode(auth_mode)
    encrypt_mode = _val_encrypt_mode(encrypt_mode)
    psk = _val_psk(psk)
    payload = {}
    if bool(name):
        payload.update(name)
    if bool(enabled):
        payload.update(enabled)
    if bool(auth_mode):
        if auth_mode['authMode'] == 'psk':
            if bool(encrypt_mode):
                payload.update(auth_mode)
                payload.update(encrypt_mode)
    if bool(psk):
        payload.update(psk)
    return payload


def Update(key='', net_id='', ssid_num=None, name='', enabled=None, auth_mode='', encrypt_mode='', psk=''):
    '''Updates the attributes of a SSID.

    Arguments:

    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: ssid_num - Service Set Identifier.
    type arg3: int

    param arg4: name - The name of the SSID.
    type arg4: str

    param arg5: enabled - Enables or disables the SSID.
    type arg5: boolean

    param arg6: auth_mode - Authentication mode only 'open' or 'psk' is accepted.
    type arg6: str ('open' or 'psk')

    param arg7: encrypt_mode - Encryption mode only accepts 'wpa' or 'wep' and
                and auth_mode is set to 'psk'.
    type arg7: str ('wpa' or 'wep')

    param arg8: psk - Pre-shared key.
    type arg8: str
    '''
    try:
        key = str(key)
        ssid_num = _val_ssid_num(ssid_num)
        payload = json.dumps(_data_join(name, enabled, auth_mode, encrypt_mode, psk))
        uri = 'https://dashboard.meraki.com/api/v0/networks/' + net_id + '/ssids/' + ssid_num
        r = requests.put(uri, data=payload, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
        return 1
