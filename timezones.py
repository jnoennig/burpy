#!/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import os

def tz_lst():
    """tz_lst imports timezones from the timezones.txt
    file into a Python list.
    """
    mydir = os.getcwd()
    with open(mydir + '/burpy/timezones.txt', 'rb') as myfile:
        newfile = myfile.read()
    timezones = newfile.split('\n')
    return timezones
