#/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import re
import json
import requests
from burpy import my_headers

def _val_vlan_id(vlan_id):
    """
    _val_vlan_id validates the vlan_id.
    """
    if bool(vlan_id) is True:
        if vlan_id > 0 and vlan_id < 4095:
            vlan_id = str(vlan_id)
            return {"id": vlan_id}
        else:
            print 'The VLAN ID must be between 1 and 4094.'
    else:
        print 'The VLAN ID must be specified and be between 1 and 4094.'

def _val_name(name):
    """
    _val_name validates the name.
    """
    if bool(name) is True:
        return {"name": name}
    else:
        print 'A name must be specified.'

def _val_subnet(subnet):
    """
    _val_subnet validates the subnet.
    """
    if bool(subnet) is True:
        regex = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}[/]\d{1,2}$")
        match = regex.search(subnet)
        if match:
            return {"subnet": subnet}
        else:
            print 'Verify subnet format. Example: 10.10.10.0/24'
    else:
        print 'A subnet must be specified.'
    

def _val_appliance_ip(appliance_ip):
    """
    _val_appliance_ip validates the appliance_ip.
    """
    if bool(appliance_ip) is True:
        regex = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
        match = regex.search(appliance_ip)
        if match:
            return {"applianceIp": appliance_ip}
        else:
            print 'Verify IP address format.'
    else:
        print 'An appliance IP must be specified.'

def _data_join(vlan_id, name, subnet, appliance_ip):
    """
    _data_join joins vlan_id, name, subnet, and appliance_ip into one string.
    """
    payload = {}
    payload.update(vlan_id)
    payload.update(name)
    payload.update(subnet)
    payload.update(appliance_ip)
    if bool(vlan_id) is True and bool(name) is True and bool(subnet) is True and bool(appliance_ip) is True:
        return payload
    else:
        print 'All parameters must have a valid value: VLAN ID, name, subnet and appliance_ip.'
    
    

def Add(key='', net_id='', vlan_id=0, name='', subnet='', appliance_ip=''):
    """Adds a VLAN to the specified network.
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: vlan_id - VLAN ID.
    type arg3: int
    
    param arg4: name - Name of VLAN.
    type arg4: str
    
    param arg5: subnet - The subnet of the VLAN.
    type arg5: str
    example: "192.168.10.0/24"
    
    param arg6: appliance_ip - Appliance IP address.
    type arg6: str
    """
    try:
        key = str(key)
        vlan_id = _val_vlan_id(vlan_id)
        name = _val_name(name)
        subnet = _val_subnet(subnet)
        appliance_ip = _val_appliance_ip(appliance_ip)
        payload = json.dumps(_data_join(vlan_id, name, subnet, appliance_ip))
        uri = "https://dashboard.meraki.com/api/v0/networks/" + net_id + "/vlans"
        r = requests.post(uri, data=payload, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
        return 1
