#/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import requests
from burpy import my_headers


def _val_vlan_id(vlan_id):
    """
    _val_vlan_id validates the vlan_id.
    """
    if bool(vlan_id) is True:
        if vlan_id > 0 and vlan_id < 4095:
            vlan_id = str(vlan_id)
            return vlan_id
        else:
            print 'The VLAN ID must be between 1 and 4094.'
    else:
        print 'The VLAN ID must be specified and be between 1 and 4094.'


def Return(key='', net_id='', vlan_id=0):
    """Returns information from the specified VLAN.
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: vlan_id - VLAN ID.
    type arg3: int
    """
    try:
        key = str(key)
        vlan_id = _val_vlan_id(vlan_id)
        uri = 'https://dashboard.meraki.com/api/v0/networks/' + net_id + '/vlans/' + vlan_id
        r = requests.get(uri, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
        return 1
