#/usr/bin/env python
#encoding: utf-8

"""
Author Name: Jason Noennig
Author Email: jnoennig@protonmail.com
Copyright 2016 Jason Noennig

This file is part of burpy.

    burpy is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    burpy is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with burpy.  If not, see <http://www.gnu.org/licenses/>.
"""

import json
import re
import requests
from burpy import my_headers

def _val_vlan_id(vlan_id):
    """
    _val_vlan_id validates the vlan_id.
    """
    if bool(vlan_id) is True:
        if vlan_id > 0 and vlan_id < 4095:
            vlan_id = str(vlan_id)
            return vlan_id
        else:
            print 'The VLAN ID must be between 1 and 4094.'
    else:
        print 'The VLAN ID must be specified and be between 1 and 4094.'

def _val_name(name):
    """
    _val_name validates the name.
    """
    if bool(name) is True:
        name = {"name": name}
        return name
    else:
        return ''

def _val_subnet(subnet):
    """
    _val_subnet validates subnet.
    """
    if bool(subnet) is True:
        regex = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}[/]\d{1,2}$")
        match = regex.search(subnet)
        if match:
            subnet = {"subnet": subnet}
            return subnet
        else:
            print 'Verify subnet format. Example: 10.10.10.0/24'
            return 1
    else:
        return ''
    

def _val_appliance_ip(appliance_ip):
    """
    _val_appliance_ip validates appliance_ip.
    """
    if bool(appliance_ip) is True:
        regex = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
        match = regex.search(appliance_ip)
        if match:
            appliance_ip = {"applianceIp": appliance_ip}
            return appliance_ip
        else:
            print 'Verify IP address format.'
            return 1
    else:
        return ''

def _data_join(name, subnet, appliance_ip):
    """
    _data_join joins name, subnet, and appliance_ip into one string.
    """
    name = _val_name(name)
    subnet = _val_subnet(subnet)
    appliance_ip = _val_appliance_ip(appliance_ip)
    payload = {}
    if bool(name) is True or bool(subnet) is True or bool(appliance_ip) is True:
        if bool(name) is True:
            payload.update(name)
        if bool(subnet) is True:
            payload.update(subnet)
        if bool(appliance_ip) is True:
            payload.update(appliance_ip)
        return payload
    else:
        print 'At least one parameter needs to be updated: name, subnet or appliance_ip.'
    
    

def Update(key='', net_id='', vlan_id=0, name='', subnet='', appliance_ip=''):
    """Updates either the name, subnet or appliance IP.
    
    Arguments:
    
    param arg1: key - Admin API key.
    type arg1: str or burpy.session.Key object
    
    param arg2: net_id - Network ID.
    type arg2: str
    
    param arg3: vlan_id - VLAN ID cannot be updated.
    type arg3: int
    
    param arg4(optional): name - Name of VLAN.
    type arg4: str
    
    param arg5(optional): subnet - The subnet of the VLAN.
    type arg5: str
    
    param arg6(optional): appliance_ip - Appliance IP address.
    type arg6: str
    """
    try:
        key = str(key)
        vlan_id = _val_vlan_id(vlan_id)
        payload = json.dumps(_data_join(name, subnet, appliance_ip))
        uri = "https://dashboard.meraki.com/api/v0/networks/" + net_id + "/vlans/" + vlan_id
        r = requests.put(uri, data=payload, headers=my_headers.headers(key))
        return (r.status_code, r.json())
    except Exception, error:
        print error
        return 1
